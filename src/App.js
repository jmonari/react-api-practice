import React, { useState } from "react";
import ReactDOM from "react-dom";
import { useEffect } from "react";
import "./app.css";
import SearchIcon from "./search.svg";
import MovieCard from "./MovieCard";
//api-key 94b4800f
const movie1 = {
  Title: "Superman/Batman: Apocalypse",
  Year: "2010",
  imdbID: "tt1673430",
  Type: "movie",
  Poster:
    "https://m.media-amazon.com/images/M/MV5BMjk3ODhmNjgtZjllOC00ZWZjLTkwYzQtNzc1Y2ZhMjY2ODE0XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg",
};

const API_KEY = "http://www.omdbapi.com?apikey=94b4800f";
const App = () => {
  const [movies, setMovies] = useState([]);
  const [searchTerm, setSearchTerm] = useState(" ");
  const searchMovies = async (title) => {
    const response = await fetch(`${API_KEY}&s=${title}`);
    const data = await response.json();

    setMovies(data.Search);
  };
  useEffect(() => {
    searchMovies("SuperMan");
  }, []);
  return (
    <div className="app">
      <h1>MovieBase</h1>
      <div className="search">
        <input
          placeholder="Search for Movies"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <img
          src={SearchIcon}
          alt="search"
          onClick={() => searchMovies(searchTerm)}
        />
      </div>

      {movies?.length > 0 ? (
        <div className="container">
          {movies.map((movie) => (
            <MovieCard movie={movie} />
          ))}
        </div>
      ) : (
        <div className="empty">
          <h2>No Movies Found</h2>
        </div>
      )}
    </div>
  );
};

export default App;
